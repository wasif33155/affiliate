<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class CountryStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = database_path()."/json/countries_state.json";

        if (File::exists($path)) {

            $file = File::get($path);

            $countries = json_decode($file);

            DB::transaction(function () use ($countries) {

                $c_id = 1;
                foreach ($countries as $key => $value) 
                {
                    DB::table('countries')->insert([
                        'id'            => $c_id,
                        'name'          => $value->name,
                        'iso3'          => $value->iso3,
                        'iso2'          => $value->iso2,
                        'phone_code'    => $value->phone_code,
                        'capital'       => $value->capital,
                        'currency'      => $value->currency,
                    ]);    

                    foreach ($value->states as $key1 => $value1) 
                    {
                        DB::table('states')->insert([
                            'country_id'      => $c_id,
                            'name'              => $value1->name,
                            'state_code'        => $value1->state_code,
                        ]);
                    };
                    $c_id++;
                }
            },3);
        }
    }
}
