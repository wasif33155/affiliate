<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestmentExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_experiences', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained();
            $table->boolean('is_stocks')->nullable()->default('0');
            $table->boolean('is_fixed_income')->nullable()->default('0');
            $table->boolean('is_options')->nullable()->default('0');
            $table->boolean('is_futures')->nullable()->default('0');
            $table->string('stock_year_experience')->nullable();
            $table->string('stock_knowledge')->nullable();
            $table->string('fixed_income_year_experience')->nullable();
            $table->string('fixed_income_knowledge')->nullable();
            $table->string('options_year_experience')->nullable();
            $table->string('options_knowledge')->nullable();
            $table->string('futures_year_experience')->nullable();
            $table->string('futures_knowledge')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_experiences');
    }
}
