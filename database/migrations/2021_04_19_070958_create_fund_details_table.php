<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained();
            $table->boolean("is_income")->default(0);
            $table->boolean("is_pension")->default(0);
            $table->boolean("is_gift")->default(0);
            $table->boolean("is_sale_of_business")->default(0);
            $table->boolean("is_inheritance")->default(0);
            $table->boolean("is_social_security")->default(0);
            $table->boolean("is_other")->default(0);
            $table->string("other_income")->nullable();
            $table->text("bank_name");
            $table->text("aba_or_swift");
            $table->text("account_number");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund_details');
    }
}
