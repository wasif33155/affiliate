<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisclosuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disclosures', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained();
            $table->boolean("agreement")->default(0);
            $table->string("initial_deposit");
            
            $table->boolean("brokage_account")->default(0);
            $table->text("brokage_account_data");

            $table->boolean("beneficial")->default(0);
            $table->text("beneficial_data");
            
            $table->boolean("shareholder")->default(0);
            $table->text("shareholder_data");

            $table->boolean("immediate")->default(0);
            $table->text("immediate_data");

            $table->boolean("securities")->default(0);
            $table->text("securities_data");

            $table->boolean("institution")->default(0);
            $table->text("institution_data");

            $table->string("tax_with_holding");
            $table->string("under_us")->nullable();
            $table->string("under_us_detail")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disclosures');
    }
}
