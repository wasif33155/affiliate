<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdentificationProofsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identification_proofs', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained();
            $table->text("first_photo_front")->nullable();
            $table->text("first_photo_back")->nullable();
            $table->text("second_photo_front")->nullable();
            $table->text("second_photo_back")->nullable();
            $table->text("third_photo_front")->nullable();
            $table->text("third_photo_back")->nullable();
            $table->text("fourth_photo_front")->nullable();
            $table->text("fourth_photo_back")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identification_proofs');
    }
}
