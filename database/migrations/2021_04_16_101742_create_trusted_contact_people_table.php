<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrustedContactPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trusted_contact_people', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained();
            $table->string("name");
            $table->string("phone");
            $table->string("email");
            $table->text("street_address");
            $table->foreignId("country_id")->constrained();
            $table->foreignId("state_id")->constrained();
            $table->text("city");
            $table->string("zip_code");
            $table->string("relation_holder");
            $table->date("date_of_birth");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trusted_contact_people');
    }
}
