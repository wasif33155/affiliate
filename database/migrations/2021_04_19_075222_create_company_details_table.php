<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_details', function (Blueprint $table) {
            $table->id();
            $table->string("company_name");
            $table->text("company_address");
            $table->string("apt_suite");
            $table->foreignId("country_id")->constrained();
            $table->foreignId("state_id")->constrained();
            $table->text("city");
            $table->string("zip_code");
            $table->string("company_email");
            $table->string("company_phone");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_details');
    }
}
