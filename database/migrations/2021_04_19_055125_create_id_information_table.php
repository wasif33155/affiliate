<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('id_information', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained();
            $table->string("tax_id");
            $table->date("date_of_birth");
            $table->foreignId("tax_country_id")->constrained("countries");
            $table->foreignId("tax_state_id")->constrained("states");
            $table->string("id_type");
            $table->string("name_of_id");
            $table->string("id_number");
            $table->foreignId("identity_country_id")->constrained("countries");
            $table->foreignId("identity_state_id")->constrained("states");
            $table->date("issue_date");
            $table->date("expiration_date");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('id_information');
    }
}
