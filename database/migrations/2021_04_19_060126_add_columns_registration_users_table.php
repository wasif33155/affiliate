<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsRegistrationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string("registration_type")->nullable()->after("affiliate_code");
            $table->string("product_you_want")->nullable()->after("registration_type");
            $table->string("hear_about_us")->nullable()->after("product_you_want");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(["registration_type","product_you_want","hear_about_us"]);
        });
    }
}
