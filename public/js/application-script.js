
//Step One Variables
var mailingCheckbox = document.querySelector('#mailing')
var contactPersonCheckbox = document.querySelector('#contact_person')
var stepOneButton = document.querySelector('#btnStepOne')
var countryStepOne = document.querySelector('#country')
var stateStepOne = document.querySelector('#state')
var mailingCountry = document.querySelector('#mailing_country')
var mailingState = document.querySelector('#mailing_state')
var contactPersonCountry = document.querySelector('#contact_person_country')
var contactPersonState = document.querySelector('#contact_person_state')

//Step Two Variables
var stepTwoButton = document.querySelector('#btnStepTwo')
var professionalDetailCountry = document.querySelector('#employee_country')
var professionalDetailState = document.querySelector('#employee_state')

//Step Three Variables 
var stepThreeButton = document.querySelector('#btnStepThree');
var threeCountry = document.querySelector('#three_country')
var threeState = document.querySelector('#three_state')
var threeResidenceCountry = document.querySelector('#three_country_residence')
var threeResidenceState = document.querySelector('#three_state_residence')
var stepFourButton = document.querySelector('#btnStepFour')
var stepFiveButton = document.querySelector('#btnStepFive')
var stepSevenButton = document.querySelector('#btnStepSeven')
var stepEightButton = document.querySelector('#btnStepEight')
var stepNineButton = document.querySelector('#btnStepNine')
var stepTenButton = document.querySelector('#btnStepTen')
var stepElevenButton = document.querySelector('#btnStepEleven')

//Step Six Variables
var stepSixButton = document.querySelector('#btnStepSix');

//core function for validation,post and dropdown filling
function gotoPreviousTab(tabNumber)
{
    $('#myTab li:nth-child('+tabNumber+') a').tab('show')
}

function fillStatesDropDown(e,t) //e element and t target dropdown
{
    $.getJSON("/fetch/states/"+e.value,function(data){
        t.innerHTML = '<option value>Select States</option>';
        $.each(data.states,function(index,val){
            var node = document.createElement("option");
            var textnode = document.createTextNode(val.name);
            node.setAttribute("value",val.id);
            node.appendChild(textnode);
            t.appendChild(node);     
            // t.appendChild(`<option value="${val.id}">${val.name}</option>`);
        })
    })
}

function printErrorMessage(errors,formId)
{
    $(formId).find("input.invalid-field").removeAttr('data-original-title').removeClass('invalid-field');
    for (var key of Object.keys(errors)) {
        fieldElement = document.querySelector('#'+key)
        if(typeof(fieldElement) != 'undefined' && fieldElement != null)
        {
            fieldElement.classList.add("invalid-field");
            fieldElement.setAttribute("data-original-title",errors[key]);
        }
  
        var element =  document.querySelector('.'+key+'-error');
        if (typeof(element) != 'undefined' && element != null)
        {
            document.querySelector('.'+key+'-error').innerHTML = errors[key];
        }
    }
    $('[data-toggle="tooltip"]').tooltip()
}

function ajaxCall(formId,url,submitbutton,redirect)
{
    $.ajax({
        url: url,
        type: "POST",
        data:  new FormData(document.querySelector(formId)),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
            submitbutton.setAttribute("disabled","disabled");
            submitbutton.children[0].style.display = 'inline';
        },
        success: function(data)
        {
            // console.log(data);
            if($.isEmptyObject(data.error)){
                $(redirect).tab('show')
            }else{
                // console.log(data.error);
                printErrorMessage(data.error,formId);
            }
        },
        error: function(e) 
        {
            console.log(e);
        },
        complete:function(){
            submitbutton.removeAttribute("disabled");
            submitbutton.children[0].style.display = 'none';
        }      
   });
}

//============= Event Listeners for StepOne
mailingCheckbox.addEventListener("change",function(){
    let formElement = document.querySelector('#mailing-div');
    if(this.checked){
        formElement.style.display = 'block';
    }else{
        formElement.style.display = 'none';
    }
})

contactPersonCheckbox.addEventListener("change",function(){
    let formElement = document.querySelector('#contact-person-div');
    if(this.checked){
        formElement.style.display = 'block';
    }else{
        formElement.style.display = 'none';
    }
})

countryStepOne.addEventListener("change",function(){
    fillStatesDropDown(this,stateStepOne);
})
mailingCountry.addEventListener("change",function(){
    fillStatesDropDown(this,mailingState);
})
contactPersonCountry.addEventListener("change",function(){
    fillStatesDropDown(this,contactPersonState);
})

stepOneButton.addEventListener("click",function(){
    ajaxCall('#steponeForm','/application/step-one',this,'#myTab li:nth-child(2) a')
})
//=================Event Listener for Step Two

professionalDetailCountry.addEventListener("change",function(){
    fillStatesDropDown(this,professionalDetailState);
})
stepTwoButton.addEventListener("click",function(){
    ajaxCall('#steptwoForm','/application/step-two',this,'#myTab li:nth-child(3) a')
})

//=================Event Listener for Step Three

threeCountry.addEventListener("change",function(){
    fillStatesDropDown(this,threeState);
})
threeResidenceCountry.addEventListener("change",function(){
    fillStatesDropDown(this,threeResidenceState);
})

stepThreeButton.addEventListener("click",function(){
    ajaxCall('#stepthreeForm','/application/step-three',this,'#myTab li:nth-child(4) a')
})
//=================Event Listener for Step Four
stepFourButton.addEventListener("click",function(){
    ajaxCall('#stepfourForm','/application/step-four',this,'#myTab li:nth-child(5) a')
})
stepFiveButton.addEventListener("click",function(){
    ajaxCall('#stepfiveForm','/application/step-five',this,'#myTab li:nth-child(6) a')
})
stepSevenButton.addEventListener("click",function(){
    ajaxCall('#stepsevenForm','/application/step-seven',this,'#myTab li:nth-child(8) a')
})

stepEightButton.addEventListener("click",function(){
    ajaxCall('#stepeightForm','/application/step-eight',this,'#myTab li:nth-child(9) a')
})

stepNineButton.addEventListener("click",function(){
    ajaxCall('#stepnineForm','/application/step-nine',this,'#myTab li:nth-child(10) a')
})

stepTenButton.addEventListener("click",function(){
    ajaxCall('#steptenForm','/application/step-ten',this,'#myTab li:nth-child(11) a')
})

stepElevenButton.addEventListener("click",function(){
    ajaxCall('#stepelevenForm','/application/step-eleven',this,'#myTab li:nth-child(11) a')
})

//=================Event Listener for Step Six
stepSixButton.addEventListener("click",function(){
    ajaxCall('#stepsixForm','/application/step-six',this,'#myTab li:nth-child(7) a')
})

$(function() {
    $('[data-toggle="tooltip"]').tooltip()
});

$(document).ready(function() {
    $("body").on('click', '#employee', function() {
        var employee = $(this).val();
        if(employee == 'employed')
        {
            $('.work_fields').css('display','block');
            $('label[for="employee_name"]').text('Employee Name*');
            $('label[for="occupation"]').text('Occupation*');
            $('label[for="employee_address"]').text('Address Of Employer*');
            $('label[for="year_of_employment"]').text('Years With Employer*');
            $('label[for="employee_phone_no"]').text('Employee Phone Number*');
        }
        else if(employee == 'self_employee')
        {
            $('.work_fields').css('display','block');
            $('label[for="employee_name"]').text('Business Name*');
            $('label[for="occupation"]').text('Company Type*');
            $('label[for="employee_address"]').text('Business Address*');
            $('label[for="year_of_employment"]').text('Years of buisness*');
            $('label[for="employee_phone_no"]').text('Business Phone Number*');
            
        }
        else if(employee == 'retired')
        {
            $('.work_fields').css('display','none');
        }
  });

  $('#funding_other').change(function(){
    if ($('#funding_other').is(':checked') == true){
        $('#funding_other_input').val('').prop('disabled', false);
    } else {
        $('#funding_other_input').prop('disabled', true);
    }
   }); 

   $('#investStock').change(function(){
    if ($('#investStock').is(':checked') == true){
        $('.stockExp').prop('disabled', false);
        $('.stockExp').get(0).checked = true;
        $('.stockExpertise').prop('disabled', false);
        $('.stockExpertise').get(0).checked = true;
    } else {
        $('.stockExp').prop('disabled', true);
        $('.stockExp').prop('checked', false);
        $('.stockExpertise').prop('disabled', false);
        $('.stockExpertise').prop('checked', false);
    }
   }); 

   $('#investBond').change(function(){
    if ($('#investBond').is(':checked') == true){
        $('.bondExp').prop('disabled', false);
        $('.bondExp').get(0).checked = true;
        $('.bondExpertise').prop('disabled', false);
        $('.bondExpertise').get(0).checked = true;
    } else {
        $('.bondExp').prop('disabled', true);
        $('.bondExp').prop('checked', false);
        $('.bondExpertise').prop('disabled', false);
        $('.bondExpertise').prop('checked', false);
    }
   }); 

   $('#investOptions').change(function(){
    if ($('#investOptions').is(':checked') == true){
        $('.optionsExp').prop('disabled', false);
        $('.optionsExp').get(0).checked = true;
        $('.optionsExpertise').prop('disabled', false);
        $('.optionsExpertise').get(0).checked = true;
    } else {
        $('.optionsExp').prop('disabled', true);
        $('.optionsExp').prop('checked', false);
        $('.optionsExpertise').prop('disabled', false);
        $('.optionsExpertise').prop('checked', false);
    }
   }); 

   $('#investFuture').change(function(){
    if ($('#investFuture').is(':checked') == true){
        $('.futureExp').prop('disabled', false);
        $('.futureExp').get(0).checked = true;
        $('.futureExpertise').prop('disabled', false);
        $('.futureExpertise').get(0).checked = true;
    } else {
        $('.futureExp').prop('disabled', true);
        $('.futureExp').prop('checked', false);
        $('.futureExpertise').prop('disabled', false);
        $('.futureExpertise').prop('checked', false);
    }
   }); 

   $('.radio_div_display').change(function(){
    var selected = $(this).val();
    var div = $(this).data('div');
    if (selected == '1'){
        $(div).css('display', 'block');
    } else if(selected == '0'){
        $(div).css('display', 'none');
    }
   }); 


});

function show1() {
    document.getElementById('undershow1').style.display = 'block';
}

function show2() {
    document.getElementById('undershow2').style.display = 'block';
}

function show3() {
    document.getElementById('undershow2').style.display = 'none';
}

function show4() {
    document.getElementById('undershow1').style.display = 'none';
    document.getElementById('undershow2').style.display = 'none';

}
$(function () {
    document.getElementById('undershow1').style.display =
        'block';
});

function changeThis(sender) { 
    if(document.getElementById('investStock').checked)
        {
            document.getElementById("stockExp").removeAttribute('disabled');
            document.getElementById("stockExp1").removeAttribute('disabled');
            document.getElementById("stockExp2").removeAttribute('disabled');

            document.getElementById("stockExpertise").removeAttribute('disabled');
            document.getElementById("stockExpertise1").removeAttribute('disabled');
            document.getElementById("stockExpertise2").removeAttribute('disabled');
            document.getElementById("stockExpertise3").removeAttribute('disabled');
        }
    else
        {
            document.getElementById("stockExp").disabled = true;
            document.getElementById("stockExp1").disabled = true;
            document.getElementById("stockExp2").disabled = true;

            document.getElementById("stockExp").removeAttribute('checked');
            document.getElementById("stockExp1").removeAttribute('checked');
            document.getElementById("stockExp2").removeAttribute('checked');

            document.getElementById("stockExpertise").disabled = true;
            document.getElementById("stockExpertise1").disabled = true;
            document.getElementById("stockExpertise2").disabled = true;
            document.getElementById("stockExpertise3").disabled = true;

            document.getElementById("stockExpertise").removeAttribute('checked');
            document.getElementById("stockExpertise1").removeAttribute('checked');
            document.getElementById("stockExpertise2").removeAttribute('checked');
            document.getElementById("stockExpertise3").removeAttribute('checked');
            $('input[type="radio"]').prop('checked', false);
        }


    if(document.getElementById('investBond').checked)
        {
            document.getElementById("bondExp").removeAttribute('disabled');
            document.getElementById("bondExp1").removeAttribute('disabled');
            document.getElementById("bondExp2").removeAttribute('disabled');

            document.getElementById("bondExpertise").removeAttribute('disabled');
            document.getElementById("bondExpertise1").removeAttribute('disabled');
            document.getElementById("bondExpertise2").removeAttribute('disabled');
            document.getElementById("bondExpertise3").removeAttribute('disabled');
        }
    else
        {
            document.getElementById("bondExp").disabled = true;
            document.getElementById("bondExp1").disabled = true;
            document.getElementById("bondExp2").disabled = true;

            document.getElementById("bondExp").removeAttribute('checked');
            document.getElementById("bondExp1").removeAttribute('checked');
            document.getElementById("bondExp2").removeAttribute('checked');

            document.getElementById("bondExpertise").disabled = true;
            document.getElementById("bondExpertise1").disabled = true;
            document.getElementById("bondExpertise2").disabled = true;
            document.getElementById("bondExpertise3").disabled = true;

            document.getElementById("bondExpertise").removeAttribute('checked');
            document.getElementById("bondExpertise1").removeAttribute('checked');
            document.getElementById("bondExpertise2").removeAttribute('checked');
            document.getElementById("bondExpertise3").removeAttribute('checked');
            $('input[type="radio"]').prop('checked', false);   
        }
    
    if(document.getElementById('investOptions').checked)
        {
            document.getElementById("optionsExp").removeAttribute('disabled');
            document.getElementById("optionsExp1").removeAttribute('disabled');
            document.getElementById("optionsExp2").removeAttribute('disabled');

            document.getElementById("optionsExpertise").removeAttribute('disabled');
            document.getElementById("optionsExpertise1").removeAttribute('disabled');
            document.getElementById("optionsExpertise2").removeAttribute('disabled');
            document.getElementById("optionsExpertise3").removeAttribute('disabled');
        }
    else
        {
            document.getElementById("optionsExp").disabled = true;
            document.getElementById("optionsExp1").disabled = true;
            document.getElementById("optionsExp2").disabled = true;

            document.getElementById("optionsExp").removeAttribute('checked');
            document.getElementById("optionsExp1").removeAttribute('checked');
            document.getElementById("optionsExp2").removeAttribute('checked');

            document.getElementById("optionsExpertise").disabled = true;
            document.getElementById("optionsExpertise1").disabled = true;
            document.getElementById("optionsExpertise2").disabled = true;
            document.getElementById("optionsExpertise3").disabled = true;

            document.getElementById("optionsExpertise").removeAttribute('checked');
            document.getElementById("optionsExpertise1").removeAttribute('checked');
            document.getElementById("optionsExpertise2").removeAttribute('checked');
            document.getElementById("optionsExpertise3").removeAttribute('checked');
            $('input[type="radio"]').prop('checked', false);   
        }


    if(document.getElementById('investFuture').checked)
        {
            document.getElementById("futureExp").removeAttribute('disabled');
            document.getElementById("futureExp1").removeAttribute('disabled');
            document.getElementById("futureExp2").removeAttribute('disabled');

            document.getElementById("futureExpertise").removeAttribute('disabled');
            document.getElementById("futureExpertise1").removeAttribute('disabled');
            document.getElementById("futureExpertise2").removeAttribute('disabled');
            document.getElementById("futureExpertise3").removeAttribute('disabled');
        }
    else
        {
            document.getElementById("futureExp").disabled = true;
            document.getElementById("futureExp1").disabled = true;
            document.getElementById("futureExp2").disabled = true;

            document.getElementById("futureExp").removeAttribute('checked');
            document.getElementById("futureExp1").removeAttribute('checked');
            document.getElementById("futureExp2").removeAttribute('checked');

            document.getElementById("futureExpertise").disabled = true;
            document.getElementById("futureExpertise1").disabled = true;
            document.getElementById("futureExpertise2").disabled = true;
            document.getElementById("futureExpertise3").disabled = true;

            document.getElementById("futureExpertise").removeAttribute('checked');
            document.getElementById("futureExpertise1").removeAttribute('checked');
            document.getElementById("futureExpertise2").removeAttribute('checked');
            document.getElementById("futureExpertise3").removeAttribute('checked');
            
            
            $('input[type="radio"]').prop('checked', false);   
        }
  }