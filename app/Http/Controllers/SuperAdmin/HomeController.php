<?php

namespace App\Http\Controllers\SuperAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
        
    }
    public function affiliate_users()
    {
        $data = User::where('affiliate_id', '=','0')->where('id', '!=','1')->orderBy('id', 'desc')->paginate(25);
        return view('admin/customers/manage')->with(['title' => 'User', 'data' => $data]);
    }

    public function users()
    {
        $data = User::where('affiliate_id','!=', '0')->orderBy('id', 'desc')->paginate(25);
        return view('admin/customers/manage')->with(['title' => 'User', 'data' => $data]);
    }
}
