<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use App\User;
use App;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Mail;
use App\Mail\ConfirmationMail;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use AuthenticatesUsers;

    protected $redirectTo = '/home';
    public function __construct()
    {
        // $this->middleware(['auth']);

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('customer.login')->with(['title'=> 'Login']);
    }

    function checklogin(Request $request)
    {

        $this->validate($request, [
            'email'   => 'required|email',
            'password'  => 'required'
        ]);

        $user_data = array(
            'email'  => $request->get('email'),
            'password' => $request->get('password')
        );

        if (Auth::attempt($user_data)) {
            // $user_data = User::where('id', Auth::user()->id);

            if( Auth::user()->is_active == 0)
            {
                return redirect()->route('verification_code');
            }

            if (!Auth::user()->hasRole(['affiliate_customer', 'customer'])) {
                $this->guard()->logout();
                $request->session()->invalidate();
                return ["error" => 'You are unauthorized to login'];
            }

            if (Auth::user()->hasRole('affiliate_customer')) {
                $redirect = route('affilate_customer.home');
            }
            if (Auth::user()->hasRole('customer')) {
                $redirect = route('application.create');
            }

            return ["success" => 'Successfully Logged In', "redirect" => $redirect];
        } else {
            return ["error" => 'Wrong User Email/Password'];
        }
    }

   function register_customer_view()
   {
        return view('customer.register')->with(['title'=> 'Register']);
   }

   function register_customer(Request $request)
   {
        $generate_verify_code = rand(100000,999999);
 
        if(User::where('email', $request->email)->count() > 0){
            return ["error" => "User Email already in use."];
        }

        // if(User::where('affiliate_code', $request->affiliate_code)->count() <= 0){
        //     return ["error" => "Incorrect Affiliate Code."];
        // }

        // $affiliate_user = User::where('affiliate_code', $request->affiliate_code)->first();
        $password                   = bcrypt($request->password);
        $data                       = new User();
        $data->name                 = @$request->name;
        $data->email                = @$request->email;
        $data->password             = @$password;
        $data->verification_code    = $generate_verify_code;
        // $data->affiliate_id = $affiliate_user->id;
        $data->save();

        // User::where('affiliate_code', $request->affiliate_code)->increment('user_count', 1);

        $role_r = Role::where('name', '=', 'customer')->firstOrFail();  
        $data->assignRole($role_r);
        Auth::login($data);
        Mail::to($request->email)->send(new ConfirmationMail('Verification Code',$generate_verify_code));
       
        return ["success" => "Successfully Added.", "redirect" => route('verification_code')];
   }

   public function activeAccount(Request $request)
   {
       $user = User::where('email',Auth::user()->email)->first();
       if($user != null)
       {
            $user->is_active = 1;
            $user->save();
            return redirect()->route('application.create');
       }
   }
}
