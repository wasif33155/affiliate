<?php

namespace App\Http\Controllers\Affiliate_customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');   
    }

    public function users()
    {
        $data = User::where('affiliate_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(25);
        return view('admin/customers/manage')->with(['title' => 'User', 'data' => $data]);
    }
}
