<?php
namespace App\Services;
use Illuminate\Support\Str;
use File;
class ImageInsertionService
{
    public function insertImage($path,$file)
    {
        $imageName = Str::random(5).'_'.time().'.'.$file->extension();
        $file->move(public_path($path), $imageName);
        return $imageName;
    }
    public function deleteImage($full_path)
    {
        File::delete(public_path($full_path));
        return true;
    }
}