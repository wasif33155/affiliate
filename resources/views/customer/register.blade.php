{{-- @extends('layouts.web')

@section('content')
<div class="container">
    <div class="row justify-content-center section-register">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

<div class="card-body">
    <form method="POST" class="ajaxForm validate" action="{{ route('customer.register.form') }}">
        @csrf

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                    value="{{ old('name') }}" required autocomplete="name" autofocus>


            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                    value="{{ old('email') }}" required autocomplete="email">

            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" required autocomplete="new-password">

            </div>
        </div>

        <div class="form-group row">
            <label for="password-confirm"
                class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required
                    autocomplete="new-password">
            </div>
        </div>

        <div class="form-group row">
            <label for="affiliate_code" class="col-md-4 col-form-label text-md-right">Affiliate Code</label>

            <div class="col-md-6">
                <input id="affiliate_code" type="text" class="form-control" name="affiliate_code" required
                    autocomplete="affiliate_code">
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="button" class="btn btn-primary ajaxFormSubmitAlter">
                    Register
                </button>
            </div>
        </div>
    </form>
</div>
</div>
</div>
</div>
</div>
@endsection

@section('footer')
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("form.validate").validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true
                },
                password: {
                    required: true
                },
                password_confirmation: {
                    required: true,
                    equalTo: "#password"
                },
                affiliate_code: {
                    required: true
                }

            },
            messages: {
                name: "This field is required.",
                email: "This field is required.",
                password: "This field is required.",
                password_confirmation: "This field is required.",
                affiliate_code: "This field is required.",

            },
            invalidHandler: function (event, validator) {
                //display error alert on form submit 

                error(validator.event);

            },
            errorPlacement: function (label, element) { // render error placement for each input type  
                $(element).addClass("border-red");
            },
            highlight: function (element) { // hightlight error inputs
                $(element).removeClass('border-green').addClass("border-red");
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).removeClass('border-red').addClass("border-green");
            },
            success: function (label, element) {
                $(element).removeClass('border-red').addClass("border-green");

            }
            // submitHandler: function (form) {
            // }
        });
    });
</script>
@endsection --}}
@extends('layouts.customer.login')

@push('custom-css')

    <style>
        label {
            font-size: 16px;
        } 
    </style>
@endpush

@section('content')
<section class="wpb_loader">
    <div class="loader"></div>
</section>

<div class="container-fluid" style="background-color: #000">
    <div class="container">
        <br /><br /><br />
        <!-- Rigister form -->
        <div class="row">
            <div class="col-md-2">
                {{-- <div class="card-header">{{ __('Login') }}</div> --}}
            </div>
            <div class="col-md-8">
                <form method="POST" class="ajaxForm validate" action="{{ route('customer.register.form') }}">
                    @csrf
                    <div class="reg_form">
                        <h3 class="text-center"><b>Begin an Online Application</b></h3>
                        <p class="text-center">
                            Already have an account? <a href="{{route('customer.login')}}">Sign in...</a>
                        </p>
    
                        <div class="form-group">
                            <label for="email">Your Email</label>
                            <input name="email" type="email" class="form-control" placeholder="Email" id="email" required=""
                                autofocus="">
                        </div>
    
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="registration_type">Registration Type</label>
                                    <select name="registrationType" id="registration_type" class="selectpicker form-control"
                                        required="">
                                        <option value="" style="display: none">
                                            Select one
                                        </option>
                                        <option value="Individual Account">
                                            Individual Account
                                        </option>
                                        <option value="JTWROS">JTWROS</option>
                                        <option value="Tenants in Common">
                                            Tenants in Common
                                        </option>
    
                                        <option value="Corporate">Corporate</option>
                                        <option value="Trust">Trust</option>
                                        <option value="Limited Liability Company">
                                            Limited Liability Company
                                        </option>
                                        <option value="Limited Partnership">
                                            Limited Partnership
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="product_trade">Product you want to trade</label>
                                    <select name="productTrade" id="product_trade" class="selectpicker form-control"
                                        required="">
                                        <option value="Stock">Stocks</option>
                                    </select>
                                </div>
                            </div>
    
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="hear_about">How did you hear about us?</label>
                                    <select name="hearAbout" id="hear_about" class="selectpicker form-control" required="">
                                        <option value="" style="display: none">
                                            Select one
                                        </option>
                                        <option value="Google">Google</option>
                                        <option value="Bloomberg">Bloomberg</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>
    
                        <div class="form-group">
                            <div class="pwdbox-holder">
                                <label for="password">Create Password </label>
                                <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                <input name="password" type="password" id="password" class="form-control"
                                    placeholder="Password"
                                    required="required">
                                {{-- <input name="password" type="password" id="password" class="form-control"
                                    placeholder="Password"
                                    title="Must contain at least one  number , one uppercase , lowercase letter, at least 8 characters, maximum 25 characters and one special characters"
                                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,25}$" required="required"> --}}
                            </div>
                        </div>
    
                        <div class="form-group">
                            <div class="pwdbox-holder">
                                <label for="passwordConfirmation">Confirm Password</label>
                                <span toggle="#passwordConfirmation"
                                    class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                    <input type="password" name="passwordConfirmation" id="passwordConfirmation"
                                    class="form-control" placeholder="Password" required="">
                                    {{-- <input type="password" name="passwordConfirmation" id="passwordConfirmation"
                                    class="form-control" placeholder="Password"
                                    title="Must contain at least one  number , one uppercase , lowercase letter, at least 8 characters, maximum 25 characters and one special characters"
                                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,25}$" required=""> --}}
                            </div>
                        </div>
                        <input id="lat" name="lat" type="hidden" class="form-control">
                        <input id="long" name="long" type="hidden" class="form-control">
                        {{-- <input type="submit" value="Sign up" class="btn btn-primary btn-block"> --}}
                        <button type="button" class="btn btn-primary btn-block ajaxFormSubmitAlter">
                            Sign Up
                        </button>
                    </div>
                </form>
            </div>
            <div class="col-md-2"></div>
        </div>
        <!-- End -->
        
        <br /><br /><br />
    </div>
</div>


@endsection

@section('footer')
<script>
    $(document).ready(function() {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
          $("form.validate").validate({
            rules: {
            
              email: {
                required: true
              },
              password: {
                required: true
              },
              passwordConfirmation:
              {
                required: true,
                equalTo : "#password"
              }
            },
            messages: {
                name: "This field is required.",
                email: "This field is required.",
                password: "This field is required.",
                passwordConfirmation: "This field is required.",
                
            },
            invalidHandler: function(event, validator) {
              //display error alert on form submit 
              error(validator.event);
            },
            errorPlacement: function(label, element) { // render error placement for each input type  
              $(element).addClass("border-red");
            },
            highlight: function(element) { // hightlight error inputs
              $(element).removeClass('border-green').addClass("border-red");
            },
            unhighlight: function(element) { // revert the change done by hightlight
              $(element).removeClass('border-red').addClass("border-green");
            },
            success: function(label, element) {
              $(element).removeClass('border-red').addClass("border-green");
            }
            // submitHandler: function (form) {
            // }
          });
        });
    </script>
{{-- <script>
    $(document).ready(function () {
        $("form.validate").validate({
            rules: {
                email: {
                    required: true
                },
                password: {
                    required: true
                },

            },
            messages: {
                email: "This field is required.",
                password: "This field is required.",

            },
            invalidHandler: function (event, validator) {
                //display error alert on form submit 
                $('#fail').show();
                $('#fail-text').html("Please fill all mandatory fields.");

            },
            errorPlacement: function (label, element) { // render error placement for each input type  
                $(element).addClass("border-red");
            },
            highlight: function (element) { // hightlight error inputs
                $(element).removeClass('border-green').addClass("border-red");
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).removeClass('border-red').addClass("border-green");
            },
            success: function (label, element) {
                $(element).removeClass('border-red').addClass("border-green");

            }
            // submitHandler: function (form) {
            // }
        });
    });

    // $('.toggle-password').click(function () {
    //     $(this).toggleClass('fa-eye fa-eye-slash');
    //     var input = $($(this).attr('toggle'));
    //     if (input.attr('type') == 'password') {
    //       input.attr('type', 'text');
    //     } else {
    //       input.attr('type', 'password');
    //     }
    //   });
</script> --}}
@endsection