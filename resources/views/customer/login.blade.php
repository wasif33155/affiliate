@extends('layouts.customer.login')

@section('content')
<section class="wpb_loader">
	<div class="loader"></div>
</section>

<div class="container-fluid" style="background-color: #000">
	<div class="container">
		<br /><br /><br />
		<!-- Rigister form -->
		<div class="row">
			<div class="col-md-2">
				{{-- <div class="card-header">{{ __('Login') }}</div> --}}
			</div>
			<div class="col-md-8">
				<form method="POST" class="ajaxForm validate" action="{{ route('customer.login-submit') }}">
					@csrf
					<div class="reg_form">
						<div class="col-md-12 logoholder">
							<img src="{{ asset('assets_customer/img/logo1.png')}}" alt="Guardian" />
						</div>

						<br />
						<h3 class="text-center"><b>User Sign In</b></h3>
						<p class="text-center">
							Don't have an account? <a href="{{route('customer.register')}}">Sign up...</a>
						</p>
						<hr />
						<input id="email" type="email" class="form-control " placeholder="Email" name="email"
							value="{{ old('email') }}" required autocomplete="email" autofocus>
						<hr />
						<input id="password" type="password" class="form-control" placeholder="Password" name="password"
							required>
						<hr />
						<button type="submit" class="btn btn-primary ajaxFormSubmitAlter">
							{{ __('Login') }}
						</button>
						<div class="forgot-pwd">
							<a href="{{route('forgot-password')}}">Forgot Password?</a>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-2"></div>
		</div>
		<!-- End -->
		<br /><br /><br />
	</div>
</div>

<!-- <div class="packages-section row container">
    <div class="row justify-content-center section-login">
        
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" class="ajaxForm validate" action="{{ route('customer.login-submit') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control " name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary ajaxFormSubmitAlter">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        
    </div>
</div> -->
@endsection

@section('footer')
<script>
	$(document).ready(function () {
		$("form.validate").validate({
			rules: {
				email: {
					required: true
				},
				password: {
					required: true
				},

			},
			messages: {
				email: "This field is required.",
				password: "This field is required.",

			},
			invalidHandler: function (event, validator) {
				//display error alert on form submit 
				$('#fail').show();
				$('#fail-text').html("Please fill all mandatory fields.");

			},
			errorPlacement: function (label, element) { // render error placement for each input type  
				$(element).addClass("border-red");
			},
			highlight: function (element) { // hightlight error inputs
				$(element).removeClass('border-green').addClass("border-red");
			},
			unhighlight: function (element) { // revert the change done by hightlight
				$(element).removeClass('border-red').addClass("border-green");
			},
			success: function (label, element) {
				$(element).removeClass('border-red').addClass("border-green");

			}
			// submitHandler: function (form) {
			// }
		});
	});
</script>
@endsection