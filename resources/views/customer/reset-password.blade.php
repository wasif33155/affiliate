
@extends('layouts.customer.login')

@section('content')
<section class="wpb_loader">
    <div class="loader"></div>
</section>
<form action="{{route('reset-password.store')}}" method="post">
	<div class="forgot-pwd">
		@csrf
		<div class="row">
			<div class="col-xl-12 mb-4">
				<input type="password" name="password" placeholder="Enter Password*" class="mb-0">
				@if($errors->any())
				<p class="text-left text-danger ml-2">{{$errors->first()}}</p>
				@endif
			</div>
			<div class="col-xl-12 mb-4">
				<input type="password" name="password_confirmation" placeholder="Enter Confirm Password*" class="mb-0">
			</div>
			<div class="col-xl-12">
				<input type="submit" value="Reset Password" >
			</div>
		</div>
	</div>
</form>