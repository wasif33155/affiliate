@extends('layouts.web')

@section('content')

<div class="packages-section row container">
    <div class="single-package silver-package col-sm-4">
        <div class="package-header">
            <h2>Silver</h2>
        </div>
        <div class="package-container">
            <ul>
                <li>Minimum No Of Users : 1500</li>
                <li>Transaction Type: Sale</li>
                <li>Commission Rate: 10 %</li>
                <li>Tracking Id: Yes</li>
                <li>Affiliate Sub-Tracking: Yes</li>
            </ul>
        </div>
        <div class="buy-btn">
            <a href="{{ route('register') }}">Sign Up</a>
        </div>
    </div>
    <div class="single-package gold-package col-sm-4">
        <div class="package-header">
            <h2>Gold</h2>
        </div>
        <div class="package-container">
            <ul>
                <li>Minimum No Of Users : 2500</li>
                <li>Transaction Type: Sale</li>
                <li>Commission Rate: 20 %</li>
                <li>Tracking Id: Yes</li>
                <li>Affiliate Sub-Tracking: Yes</li>
            </ul>
        </div>
        <div class="buy-btn">
            <a href="{{ route('register') }}">Sign Up</a>
        </div>
    </div>
    <div class="single-package platinum-package col-sm-4">
        <div class="package-header">
            <h2>Platinum</h2>
        </div>
        <div class="package-container">
            <ul>
                <li>Minimum No Of Users : 5000</li>
                <li>Transaction Type: Sale</li>
                <li>Commission Rate: 30 %</li>
                <li>Tracking Id: Yes</li>
                <li>Affiliate Sub-Tracking: Yes</li>
            </ul>
        </div>
        <div class="buy-btn">
            <a href="{{ route('register') }}">Sign Up</a>
        </div>
    </div>

</div>
@endsection