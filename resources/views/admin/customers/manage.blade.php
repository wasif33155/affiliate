@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">DataTable with minimal features & hover style</h3>
    </div>
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Name</th>
                    
                    
                </tr>
            </thead>
            <tbody>
                @php ($count = 1)
                @foreach( $data as $v)
                <tr>
                    <td>{{ $count++ }}</td>
                    <td>{{ $v->name }}</td>
                    
                </tr>
                @endforeach
            </tbody>
           
        </table>
    </div>
</div>


@endsection
@section('footer')
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>
@endsection